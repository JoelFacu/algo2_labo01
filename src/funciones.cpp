#include <vector>
#include "algobot.h"

using namespace std;

// Ejercicio 1

int apariciones(vector<int> s,int h){
    int apar = 0;
    for(int i=0;i<s.size();i++){
        if(s[i]==h){
            apar++;
        }
    }
    return apar;
}
vector<int> quitar_repetidos(vector<int> s) {
    vector<int> sinrepe;
    for(int i=0; i<s.size();i++){
        if(apariciones(s,s[i])>1){
            if(apariciones(sinrepe,s[i])==0){
                sinrepe.push_back(s[i]);
            }
        }else{
            sinrepe.push_back(s[i]);
        }

    }
    return sinrepe;
}

// Ejercicio 2


//set_sinrepe.count(s[i])==0
//set_a.insert(a[i]);



vector<int> quitar_repetidos_v2(vector<int> s) {
    vector<int> sinrepe;
    set<int> set_a;
    for(int i=0; i<s.size();i++){
        if(apariciones(s,s[i])>1){
            if(set_a.count(s[i]) == 0){
                sinrepe.push_back(s[i]);
                set_a.insert(s[i]);
            }
        }else{
            sinrepe.push_back(s[i]);
            set_a.insert(s[i]);
        }

    }
    return sinrepe;
}

// Ejercicio 3
bool mismos_elementos(vector<int> a, vector<int> b) {
    int j=0;
    for(int i=0;i<a.size();i++){
        if(apariciones(b,a[i])>=1){
            j++;
        }
    }for(int m=0;m<b.size();m++){
        if(apariciones(a,b[m])>=1){
            j++;
        }
    }
    return j==a.size()+b.size();
}

// Ejercicio 4
bool mismos_elementos_v2(vector<int> a, vector<int> b) {
    int j=0;
    set<int> set_a;
    set<int> set_b;
    for(int i=0;i<a.size();i++){
        set_a.insert(a[i]);
    }for(int m=0;m<b.size();m++){
        if (set_a.count(b[m])==1){
            j++;
        }
    }
    for(int k=0;k<b.size();k++){
        set_b.insert(b[k]);
    }for(int n=0;n<a.size();n++) {
        if (set_b.count(a[n]) == 1) {
            j++;
        }
    }
    return  j==(b.size()+a.size());
}

// Ejercicio 5
//{numero,cuantavecesaparecenumero}

map<int, int> contar_apariciones(vector<int> s) {
    map<int, int> cp;
    for(int i=0;i<s.size();i++){
        cp[s[i]]=apariciones(s,s[i]);
    }
    return cp;
}

// Ejercicio 6
vector<int> filtrar_repetidos(vector<int> s) {
    vector<int> sinrepe;
    for(int i=0; i<s.size();i++){
        if(apariciones(s,s[i])==1){
            sinrepe.push_back(s[i]);
        }
    }
    return sinrepe;
}

// Ejercicio 7
//set_sinrepe.count(s[i])==0
//set_a.insert(a[i]);
set<int> interseccion(set<int> a, set<int> b) {
    set<int> c;
    vector<int> a1;
    vector<int> b1;
    vector<int> c1;
    for (int n : a){
        a1.push_back(n);
    }
    for (int m:b){
        b1.push_back(m);
    }
    for(int i=0;i<a1.size();i++){
        if (apariciones(b1,a1[i])>=1){
            c1.push_back(a1[i]);
        }

    }
    for(int j=0;j<b1.size();j++){
        if(apariciones(a1,b1[j])>=1){
            c1.push_back(b1[j]);
        }
    }
    quitar_repetidos(c1);
    for(int h=0;h<c1.size();h++){
        c.insert(c1[h]);
    }
    return c;
}

// Ejercicio 8

set<int> resultadosUnidad(vector<int> s,int h){
    set<int> Unidad;
    for(int i=0;i<s.size();i++){
        if (h==s[i]%10){
            Unidad.insert(s[i]);
        }
    }
    return Unidad;
}


int aparicionesPorUnidad(vector<int> s,int h){
    int aparU = 0;
    for(int i=0;i<s.size();i++){
        if(s[i]%10==h){
            aparU++;
        }
    }
    return aparU;
}


map<int, set<int>> agrupar_por_unidades(vector<int> s) {
    map<int, set<int>> res;
    for(int i=0;i<s.size();i++){
        if(aparicionesPorUnidad(s,s[i]%10)>1){
            res[s[i] % 10 ] = resultadosUnidad(s,s[i] % 10);
        }else{
            set<int> resi;
            resi.insert(s[i]);
            res[s[i]%10]=resi;
        }
    }

    return res;
}

// Ejercicio 9
vector<char> traducir(vector<pair<char, char>> tr, vector<char> str) {
    vector<char> nuevo;
    nuevo = str;
    for(int i=0;i<tr.size();i++){
        char c=tr[i].first;
        char d=tr[i].second;
        tr[i].first=tr[i].second;
        for(int j=0;j<str.size();j++){
            if(c==str[j]){
                nuevo[j]=tr[i].first;
            }
        }
    }
    return nuevo;
}

// Ejercicio 10
bool tienenLuIguales(string m , string s){
    int verdad1=0;
    int verdad2=0;
    for(int i=0;i<6;i++){
        if (m[i]==s[i]){
            verdad1++;
        }
    }
    for(int i=7;i<13;i++){
        if (m[i]==s[i]){
            verdad2++;
        }
    }
    return (verdad1==6 || verdad2==6);
}


bool integranteRepetido(Mail m, vector<Mail> s){
    int verdad=0;
    for(int i=0;i<s.size()-1;i++){
        if( tienenLuIguales(m.asunto(),s[i+1].asunto())){
            verdad++;
        }
    }
    return verdad>0;
}


bool integrantes_repetidos(vector<Mail> s) {
    int j=0;
    for(int i=0;i<s.size();i++){
        if( integranteRepetido(s[i],s)){
            j++;
        }
    }
    return j!=0;
}

// Ejercicio 11


map<set<LU>, Mail> entregas_finales(vector<Mail> s) {
    map<set<LU>, Mail> diccionario;
    for(int i =0;i<s.size();i++){
        if(s[i].adjunto()){
            diccionario[s[i].libretas()]=s[s.size()-1];
        }
    }
    return diccionario;
}



